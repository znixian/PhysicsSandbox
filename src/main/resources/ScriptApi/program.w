
// TODO implement modules
// import \"api\" for Microcontroller

var mc = Microcontroller.me
var craft = mc.craft

// PIDs
var vert = PIDSet.new()
vert.add(1.5, 1, 0.1, 0, 1)
vert.add(0.5, 0, 0.05)
vert.target = 5

var horizontal = PIDSet.new()
horizontal.add(0.5, 0.01, 2)
horizontal.add(0.5, 0.0, 0.5, 0.5)
horizontal.target = 5

var tr = craft.thrusters
var t1 = tr[0]
var t2 = tr[1]
var t3 = tr[2]

while(true) {
	// System.print(craft.position.z)
	if(craft.position.z < -15) {
		horizontal.target = horizontal.target.abs
		vert.target = 15
	} else if(craft.position.z > 15) {
		horizontal.target = -horizontal.target.abs
		vert.target = 0
	}

	t2.thrust = vert.step(mc.time, [craft.velocity.y, craft.position.y], 1)

	var bal = horizontal.step(mc.time, [craft.rotation.up.z, craft.velocity.z]) * 0.3

	t1.thrust = 0.3+bal
	t3.thrust = 0.3-bal
}
