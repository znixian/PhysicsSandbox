
class Base {
	foreign toString
}

foreign class Microcontroller is Base {
	foreign static me

	foreign craft
	foreign time
}

// TODO move to another file
foreign class Craft is Base {
	foreign position
	foreign velocity
	foreign rotation
	foreign angularVelocity
	foreign thrusters
}

foreign class Pos is Base {
	construct new() {}
	construct new(ix, iy, iz) {
		x = ix
		y = iy
		z = iz
	}

	foreign x
	foreign y
	foreign z

	foreign x=(v)
	foreign y=(v)
	foreign z=(v)
}

foreign class Thruster is Base {
	foreign thrust
	foreign thrust=(val)
}

foreign class PID is Base {
	foreign construct new()
	foreign construct new(target)

	foreign target
	foreign target=(val)

	foreign kp
	foreign ki
	foreign kd

	foreign kp=(val)
	foreign ki=(val)
	foreign kd=(val)

	foreign imult
	foreign imin
	foreign imax

	foreign imult=(val)
	foreign imin=(val)
	foreign imax=(val)

	foreign min
	foreign max
	foreign min=(val)
	foreign max=(val)

	foreign step(time, value)
	foreign step(time, value, logging)
}

foreign class Rotation is Base {
	foreign forward
	foreign up
}

class PIDSet {
	construct new() {
		_pids = []
	}

	add(val) {
		if(!(val is PID)) {
			Fiber.abort("Something bad happened")
		}

		_pids.add(val)
	}

	add(p, i, d) {
		add(p, i, d, 1)
	}

	add(p, i, d, scale) {
		add(p, i, d, -scale, scale)
	}

	add(p, i, d, min, max) {
		var pid = PID.new()
		pid.kp = p
		pid.ki = i
		pid.kd = d
		pid.min = min
		pid.max = max
		add(pid)
	}

	[i] {
		System.print("hi!")
		return 0
	}

	target {
		return _pids[-1].target
	}

	target=(val) {
		_pids[-1].target = val
	}

	step(time, values) {
		return step(time, values, -1)
	}

	step(time, values, index) {
		var val
		var starting = true
		for(i in _pids.count-1 .. 0) {
			var pid = _pids[i]
			if(!starting) {
				pid.target = val
			}
			starting = false
			val = pid.step(time, values[i], index == i)
		}
		return val
	}
}
