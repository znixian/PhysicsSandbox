/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.physicssandbox.math;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class PIDLoop {

	/**
	 * The set point (target value).
	 */
	public double target;

	/**
	 * The accumulated bias for the integral term.
	 */
	public double integral;

	/**
	 * The value last update (used for the derivative term).
	 */
	public double lastValue;

	/**
	 * Gains (tuning parameters).
	 */
	public double Kp, Ki, Kd;

	/**
	 * The integrator tuning parameters: multiplier, minimum, and maximum.
	 */
	public double Imult = 1, Imin = -100, Imax = 100;

	public boolean firstIteration = true;

	public double update(double tpf, double input) {
		return update(tpf, input, false);
	}

	public double update(double delta, double input, boolean logging) {

		// Calculate basic values
		double error = target - input;

		// Calculate the result parts
		double pout = Kp * error;
		double iout = Ki * integral;
		double dout = Kd * (lastValue - input) / delta;

		// Apply the intergral and derivitive
		integral += error * delta * Imult;
		lastValue = input;

		// TODO configurable integral caps
		integral = Math.max(Imin, Math.min(Imax, integral));

		// Don't apply D on the first iteraton, or if it will be
		// a NaN value (from div by 0)
		if (firstIteration || delta == 0) {
			dout = 0;
		}
		firstIteration = false;

		// Calculate and return the final result
		double out = pout + iout + dout;
		out = Math.max(-1, Math.min(1, out));

		// CSV logger:
		if (logging) {
			System.out.format("%f,%f,%f,%f,%f,%f,%f\n", target, error, out, pout, iout, dout, integral);
		}

		return out;
	}
}
