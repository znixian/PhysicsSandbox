package xyz.znix.physicssandbox;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.input.FlyByCamera;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Node;
import xyz.znix.physicssandbox.craft.BlockBlueprint;
import xyz.znix.physicssandbox.craft.BlockType;
import xyz.znix.physicssandbox.craft.CraftDescriptor;
import xyz.znix.physicssandbox.craft.Vector3i;

import static com.jme3.input.MouseInput.*;
import static xyz.znix.physicssandbox.craft.BlockInformationProvider.BLOCK_SIZE;

/**
 * Created by znix on 27/06/17.
 */
public class EditorAppState extends AbstractAppState {
	private App application;
	private Node rootNode;

	private BlockType selectedType = BlockType.STRUCTURAL;

	private CraftDescriptor craft;

	public EditorAppState(CraftDescriptor craft) {
		this.craft = craft;
	}

	@Override
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);
		application = (App) app;
		setupScene();
		setupCamera();
		application.getRootNode().attachChild(rootNode);

		InputManager in = application.getInputManager();
		in.addMapping("AddBlock", new MouseButtonTrigger(BUTTON_LEFT));
		in.addMapping("DelBlock", new MouseButtonTrigger(BUTTON_RIGHT));
		in.addListener((ActionListener) this::mouseAction, "AddBlock", "DelBlock");

		in.addMapping("NextType", new KeyTrigger(KeyInput.KEY_P));
		in.addMapping("PrevType", new KeyTrigger(KeyInput.KEY_O));
		in.addListener((ActionListener) this::selectBlockEvent, "NextType", "PrevType");

		updateCraft();
	}

	private void setupCamera() {
		FlyByCamera cam = application.getFlyByCamera();
		cam.setDragToRotate(true);
	}

	@Override
	public void cleanup() {
		super.cleanup();
		application.getRootNode().detachChild(rootNode);

		InputManager in = application.getInputManager();
		in.deleteMapping("AddBlock");
		in.deleteMapping("DelBlock");
		in.deleteMapping("NextType");
		in.deleteMapping("PrevType");
	}

	private void setupScene() {
		rootNode = new Node("EditorRootNode");
	}

	@Override
	public void update(float tpf) {
		super.update(tpf);
	}

	@Override
	public void render(RenderManager rm) {
		super.render(rm);
	}

	public CraftDescriptor getCraft() {
		return craft;
	}

	private void updateCraft() {
		rootNode.detachAllChildren();

		craft.createNodes(application.getAssetResolver(), rootNode::attachChild);
	}

	private void selectBlockEvent(String name, boolean isPressed, float tpf) {
		if (!isPressed) return;

		int index = selectedType.ordinal();

		if ("NextType".equals(name)) {
			index++;
		} else {
			index--;
		}

		BlockType[] vals = BlockType.values();
		if (index >= vals.length) {
			index = 0;
		} else if (index < 0) {
			index = vals.length - 1;
		}

		selectedType = vals[index];
		System.out.println(selectedType);
	}

	private void mouseAction(String name, boolean isPressed, float tpf) {
		if (!isPressed) return;
		if (!application.getFlyByCamera().isMouseFree()) return;

		Camera cam = application.getCamera();

		// Reset results list.
		CollisionResults results = new CollisionResults();

		{
			// Create a ray based on where on the screen the user clicked
			// Info about the Z buffer: http://www.sjbaker.org/steve/omniv/love_your_z_buffer.html

			// 1. Find the 2d position the user clicked at
			Vector2f click2d = application.getInputManager().getCursorPosition().clone();

			// Find a point 'pressed up against the lens', the closest to the camera that will be rendered
			Vector3f hither = cam.getWorldCoordinates(
					click2d, 0f).clone();

			// Find a point, also under the mouse, that is the furthest point that OpenGL will still render.
			Vector3f yonder = cam.getWorldCoordinates(click2d, 1f);

			// Find a point in the infinite distance
			Vector3f dir = yonder.subtract(hither).normalizeLocal();

			// Make a ray between these points
			Ray ray = new Ray(hither, dir);

			// Find the collision results
			rootNode.collideWith(ray, results);
		}

		// Use the results
		if (results.size() > 0) {
			// The closest collision point is what was truly hit:
			CollisionResult closest = results.getClosestCollision();

			Vector3i innerBlockLocation = new Vector3i(
					closest.getGeometry().getLocalTranslation().divide(BLOCK_SIZE)
			);
			Vector3i dir = new Vector3i(closest.getContactNormal());

			Vector3i outerBlockLocation = innerBlockLocation.add(dir);

			if ("AddBlock".equals(name)) {
				BlockBlueprint blockBlueprint = selectedType.create();
				craft.getBlocks().put(outerBlockLocation, blockBlueprint);
			} else {
				craft.getBlocks().remove(innerBlockLocation);
			}

			updateCraft();
		}
	}
}
