package xyz.znix.physicssandbox;

import com.jme3.input.CameraInput;
import com.jme3.input.FlyByCamera;
import com.jme3.input.InputManager;
import com.jme3.renderer.Camera;

/**
 * Created by znix on 7/1/17.
 */
public class CustomFlyByCamera extends FlyByCamera {

	public CustomFlyByCamera(Camera cam, InputManager inputManager) {
		super(cam);
		this.inputManager = inputManager;
	}

	@Override
	public void registerWithInput(InputManager inputManager) {
		inputManager.addListener(this,
				CameraInput.FLYCAM_LEFT,
				CameraInput.FLYCAM_RIGHT,
				CameraInput.FLYCAM_UP,
				CameraInput.FLYCAM_DOWN,

				CameraInput.FLYCAM_STRAFELEFT,
				CameraInput.FLYCAM_STRAFERIGHT,
				CameraInput.FLYCAM_FORWARD,
				CameraInput.FLYCAM_BACKWARD,

				CameraInput.FLYCAM_ROTATEDRAG,

				CameraInput.FLYCAM_RISE,
				CameraInput.FLYCAM_LOWER,

				CameraInput.FLYCAM_INVERTY);

		inputManager.setCursorVisible(dragToRotate || !isEnabled());
	}

	public boolean isMouseFree() {
		return dragToRotate && !canRotate;
	}
}
