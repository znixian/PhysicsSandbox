package xyz.znix.physicssandbox;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.collision.*;
import com.jme3.input.FlyByCamera;
import com.jme3.input.controls.ActionListener;
import com.jme3.material.Material;
import com.jme3.math.*;
import com.jme3.renderer.Camera;
import com.jme3.scene.*;
import com.jme3.scene.shape.Box;
import xyz.znix.physicssandbox.craft.Block;
import xyz.znix.physicssandbox.craft.BlockInformationProvider;
import xyz.znix.physicssandbox.craft.CraftDescriptor;
import xyz.znix.physicssandbox.craft.NodeBasedCraft;
import xyz.znix.physicssandbox.craft.SimulatedCraft;
import xyz.znix.physicssandbox.craft.Vector3i;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static xyz.znix.physicssandbox.App.INPUT_FOCUS;
import static xyz.znix.physicssandbox.craft.BlockInformationProvider.BLOCK_SIZE;

/**
 * Created by znix on 27/06/17.
 */
public class SimulateAppState extends AbstractAppState implements ActionListener {
	private final List<CraftDescriptor> craftDescriptors;

	private BulletAppState physics;
	private App application;
	private Node rootNode;
	private List<SimulatedCraft> crafts;

	private SimulatedCraft following;
	private final Vector3f lastFollowedObjPos = new Vector3f();

	public SimulateAppState(List<CraftDescriptor> crafts) {
		craftDescriptors = crafts;
		physics = new BulletAppState();
	}

	@Override
	public void initialize(AppStateManager stateManager, Application app) {
		super.initialize(stateManager, app);
		this.application = (App) app;
		stateManager.attach(physics);
		initCamera();

		rootNode = new Node();
		application.getRootNode().attachChild(rootNode);
		initFloor();
		crafts = new ArrayList<>();
		craftDescriptors.forEach(this::initCraft);

		application.getInputManager().addListener(this, INPUT_FOCUS);
	}

	private void initCamera() {
		FlyByCamera cam = application.getFlyByCamera();
		cam.setDragToRotate(false);
	}

	@Override
	public void cleanup() {
		application.getRootNode().detachChild(rootNode);
		application.getStateManager().detach(physics);
		application.getInputManager().removeListener(this);
		application = null;
		rootNode = null;
		super.cleanup();
	}

	@Override
	public void update(float tpf) {
		for (SimulatedCraft craft : crafts) {
			Vector3f com = craft.getCenterOfMass();
			Vector3f physicalPos = new Vector3f();

			for (Map.Entry<Vector3i, Block> entry : craft.getBlocks().entrySet()) {
				if (entry.getValue().getBlueprint().isTickable()) {
					Vector3i pos = entry.getKey();

					pos.toFloat(physicalPos);
					physicalPos.multLocal(BLOCK_SIZE);
					physicalPos.subtractLocal(com);

					entry.getValue().update(craft, pos, physicalPos, tpf);
				}
			}
		}

		if (following != null) {
			Vector3f movement = new Vector3f();
			Vector3f position = following.getNode().getWorldTranslation();

			movement.set(position);
			movement.subtractLocal(lastFollowedObjPos);

			Camera cam = application.getCamera();
			cam.getLocation().addLocal(movement);

			// We manually edited the location, we have to update the frame parameters.
			cam.onFrameChange();

			lastFollowedObjPos.set(position);
		}
	}

	private void initFloor() {
		Material floor_mat = new Material(application.getAssetManager(),
				"Common/MatDefs/Misc/Unshaded.j3md");
		Geometry floor_geo = new Geometry("Floor", new Box(10f, 0.1f, 5f));
		floor_geo.setMaterial(floor_mat);
		floor_geo.setLocalTranslation(0, -10.1f, 0);
		floor_geo.rotate((float) Math.toRadians(30), 0, 0);
		this.rootNode.attachChild(floor_geo);

		// Make the floor physical with mass 0.0f!
		RigidBodyControl floor_phy = new RigidBodyControl(0.0f);
		floor_geo.addControl(floor_phy);
		physics.getPhysicsSpace().add(floor_phy);
	}

	private void initCraft(CraftDescriptor craft) {
		Vector3f com = craft.getCenterOfMass().mult(BLOCK_SIZE);
		Node node = createModelAndPhysics(craft.getMass(), com, craft.getBlocks());

		// Create a simulated crafts for this
		RigidBodyControl rbc = node.getControl(RigidBodyControl.class);
		SimulatedCraft result = new SimulatedCraft(craft, rbc, node, com);

		// Add in the craft
		rootNode.attachChild(node);
		crafts.add(result);
		physics.getPhysicsSpace().add(rbc);

		updateCraft(result);
	}

	private Node createModelAndPhysics(float mass, Vector3f com,
									   Map<Vector3i, ? extends BlockInformationProvider> blocks) {
		// Scale up the center of mass to the world scale
		final Vector3f worldCom = com;

		// A node to put everything on
		Node craftNode = new Node("Craft");

		// Put each block onto our craft node.
		NodeBasedCraft.createNodes(application.getAssetResolver(), blocks, spat -> {
			// We want to position them relative to the center of mass.
			spat.getLocalTranslation().subtractLocal(worldCom);

			// Add the node to the scene
			craftNode.attachChild(spat);
		});

		// Add physics
		RigidBodyControl rbc = new RigidBodyControl(mass);
		craftNode.addControl(rbc);
		rbc.setPhysicsLocation(com);

		// Hand off our craft
		return craftNode;
	}

	private void updateCraft(SimulatedCraft craft) {
		List<? extends Collection<Vector3i>> blockSets = craft.findAllCrafts();
		if (blockSets == null) return;

		physics.getPhysicsSpace().remove(craft.getPhysics());
		crafts.remove(craft);
		rootNode.detachChild(craft.getNode());

		for (Collection<Vector3i> newCraftBlocks : blockSets) {
			Map<Vector3i, Block> blocks = new HashMap<>();
			for (Vector3i pos : newCraftBlocks) {
				Block block = craft.getBlocks().get(pos);

				blocks.put(pos, block);
			}

			Vector3f com = NodeBasedCraft.getCenterOfMass(blocks).mult(BLOCK_SIZE);
			float mass = NodeBasedCraft.getMass(blocks);
			Node node = createModelAndPhysics(mass, com, blocks);

			RigidBodyControl rbc = node.getControl(RigidBodyControl.class);

			SimulatedCraft newCraft = new SimulatedCraft(rbc, node, blocks, com);

			// Add the craft to the world
			crafts.add(newCraft);
			rootNode.attachChild(node);
			physics.getPhysicsSpace().add(rbc);
		}
	}

	@Override
	public void onAction(String name, boolean isPressed, float tpf) {
		switch (name) {
			case INPUT_FOCUS:
				if (isPressed) {
					if (following != null) {
						following = null;
					} else {
						updateFollowing(tpf);
					}
				}
				break;
			default:
				throw new IllegalStateException("Bad input " + name);
		}
	}

	private void updateFollowing(float tpf) {
		Camera cam = application.getCamera();

		CollisionResults results = new CollisionResults();
		Ray ray = new Ray(cam.getLocation(), cam.getDirection());
		rootNode.collideWith(ray, results);

		if (results.size() <= 0) {
			return;
		}

		CollisionResult closest = results.getClosestCollision();
		Spatial geom = closest.getGeometry();

		while (geom.getParent() != rootNode) {
			geom = geom.getParent();
			if (geom == null) return;
		}

		SimulatedCraft craft = null;

		for (SimulatedCraft candidate : crafts) {
			if (candidate.getNode() == geom) {
				craft = candidate;
			}
		}

		if (craft == null) return;

		following = craft;
		lastFollowedObjPos.set(following.getNode().getWorldTranslation());
	}
}
