package xyz.znix.physicssandbox;

import com.jme3.app.SimpleApplication;
import com.jme3.app.StatsAppState;
import com.jme3.app.state.AppState;
import com.jme3.audio.AudioListenerState;
import com.jme3.input.CameraInput;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.material.Material;
import com.jme3.math.FastMath;
import com.jme3.texture.Texture;
import xyz.znix.physicssandbox.craft.CraftDescriptor;
import xyz.znix.physicssandbox.craft.NodeBasedCraft;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * JME Application / Main Class
 * <p>
 * Created by znix on 27/06/17.
 */
public class App extends SimpleApplication implements ActionListener {
	public static final String INPUT_FOCUS = "Focus";

	private EditorAppState editor;
	private SimulateAppState simulation;
	private SaveSystem save;
	private NodeBasedCraft.AssetResolver assetResolver;
	private CustomFlyByCamera flyCam;

	private int moveSpeed = 1;

	public static void main(String[] args) {
		App app = new App();
		app.start();
	}

	public App() {
		super(new StatsAppState(), new AudioListenerState());
	}

	@Override
	public void simpleInitApp() {
		assetResolver = createAssetResolver();
		save = new SaveSystem();
		CraftDescriptor craft = save.load();
		if (craft == null) craft = new CraftDescriptor();
		loadCraftToEditor(craft);

		// Set everything up
		setupInput();
		setupCamera();

		editMode();
	}

	private void setupInput() {
		inputManager.addMapping("Run", new KeyTrigger(KeyInput.KEY_F1));
		inputManager.addMapping("Stop", new KeyTrigger(KeyInput.KEY_F2));

		inputManager.addMapping("Save", new KeyTrigger(KeyInput.KEY_F5));
		inputManager.addMapping("Load", new KeyTrigger(KeyInput.KEY_F9));

		inputManager.addMapping("Speed+", new KeyTrigger(KeyInput.KEY_X));
		inputManager.addMapping("Speed-", new KeyTrigger(KeyInput.KEY_Z));

		inputManager.addMapping(INPUT_FOCUS, new KeyTrigger(KeyInput.KEY_TAB));

		inputManager.deleteMapping(INPUT_MAPPING_HIDE_STATS);
		inputManager.addMapping(INPUT_MAPPING_HIDE_STATS, new KeyTrigger(KeyInput.KEY_M));

		inputManager.addListener(this,
				"Run", "Stop", "Save", "Load", "Speed+", "Speed-",
				INPUT_MAPPING_HIDE_STATS);

		// Set up the camera mappings
		inputManager.addMapping(CameraInput.FLYCAM_ROTATEDRAG, new KeyTrigger(KeyInput.KEY_LSHIFT));

		// both mouse and button - rotation of cam
		inputManager.addMapping(CameraInput.FLYCAM_LEFT, new MouseAxisTrigger(MouseInput.AXIS_X, true),
				new KeyTrigger(KeyInput.KEY_LEFT));

		inputManager.addMapping(CameraInput.FLYCAM_RIGHT, new MouseAxisTrigger(MouseInput.AXIS_X, false),
				new KeyTrigger(KeyInput.KEY_RIGHT));

		inputManager.addMapping(CameraInput.FLYCAM_UP, new MouseAxisTrigger(MouseInput.AXIS_Y, false),
				new KeyTrigger(KeyInput.KEY_UP));

		inputManager.addMapping(CameraInput.FLYCAM_DOWN, new MouseAxisTrigger(MouseInput.AXIS_Y, true),
				new KeyTrigger(KeyInput.KEY_DOWN));

		// keyboard only WASD for movement and WZ for rise/lower height
		inputManager.addMapping(CameraInput.FLYCAM_STRAFELEFT, new KeyTrigger(KeyInput.KEY_A));
		inputManager.addMapping(CameraInput.FLYCAM_STRAFERIGHT, new KeyTrigger(KeyInput.KEY_D));
		inputManager.addMapping(CameraInput.FLYCAM_FORWARD, new KeyTrigger(KeyInput.KEY_W));
		inputManager.addMapping(CameraInput.FLYCAM_BACKWARD, new KeyTrigger(KeyInput.KEY_S));
		inputManager.addMapping(CameraInput.FLYCAM_RISE, new KeyTrigger(KeyInput.KEY_Q));
		inputManager.addMapping(CameraInput.FLYCAM_LOWER, new KeyTrigger(KeyInput.KEY_E));
	}

	@Override
	public void onAction(String name, boolean isPressed, float tpf) {
		if (!isPressed) return;

		switch (name) {
			case "Run":
				runMode();
				break;
			case "Stop":
				editMode();
				break;
			case "Save":
				save.save(editor.getCraft());
				break;
			case "Load": {
				editMode();
				CraftDescriptor loaded = save.load();
				if (loaded != null) loadCraftToEditor(loaded);
				break;
			}
			case INPUT_MAPPING_HIDE_STATS:
				stateManager.getState(StatsAppState.class).toggleStats();
				break;
			case "Speed+":
				changeMoveSpeed(1);
				break;
			case "Speed-":
				changeMoveSpeed(-1);
				break;
			default:
				throw new IllegalArgumentException("Bad action " + name);
		}
	}

	private void changeMoveSpeed(int amt) {
		moveSpeed += amt;
		flyCam.setMoveSpeed(FastMath.pow(Constants.BASE_MOVE_SPEED, moveSpeed * Constants.EXP_MOVE_SPEED_MULT));
	}

	private void setupCamera() {
		flyCam = new CustomFlyByCamera(cam, inputManager);
		flyCam.registerWithInput(inputManager);
	}

	public void loadCraftToEditor(CraftDescriptor craft) {
		clearStates(s -> true);
		simulation = null;

		editor = new EditorAppState(craft);

		stateManager.attach(editor);
	}

	private void clearStates(Predicate<AppState> shouldRemove) {
		Stream.of(
				editor,
				simulation
		)
				.filter(Objects::nonNull)
				.filter(shouldRemove)
				.forEach(stateManager::detach);

		if (!stateManager.hasState(simulation)) {
			simulation = null;
		}
	}

	public void editMode() {
		clearStates(s -> s != editor);
		stateManager.attach(editor);

		simulation = null;
	}

	public void runMode() {
		clearStates(s -> true);

		simulation = new SimulateAppState(Collections.singletonList(editor.getCraft()));

		stateManager.attach(simulation);
	}

	@Override
	public CustomFlyByCamera getFlyByCamera() {
		return flyCam;
	}

	public NodeBasedCraft.AssetResolver getAssetResolver() {
		return assetResolver;
	}

	private NodeBasedCraft.AssetResolver createAssetResolver() {
		Material baseMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		Map<String, Texture> textures = new HashMap<>();

		return new NodeBasedCraft.AssetResolver() {
			@Override
			public Texture getTexture(String name) {
				return textures.computeIfAbsent(name, assetManager::loadTexture);
			}

			@Override
			public Material getMaterial(String name) {
				// TODO implement
				return baseMat;
			}
		};
	}
}
