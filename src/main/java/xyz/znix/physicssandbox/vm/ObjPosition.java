/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.physicssandbox.vm;

import com.jme3.math.Vector3f;
import io.wren.vm.*;

import static xyz.znix.physicssandbox.vm.Utils.*;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class ObjPosition extends ObjBase {

	private final Vector3f position;
	private final boolean mutable;

	private ObjPosition(Pak pak, WrenVM vm) {
		this(pak, vm, new Vector3f(), true);
	}

	public ObjPosition(Pak pak, WrenVM vm, Vector3f position) {
		this(pak, vm, position, false);
	}

	public ObjPosition(Pak pak, WrenVM vm, Vector3f position, boolean mutable) {
		super(pak, vm, pak.position);
		this.position = position;
		this.mutable = mutable;
	}

	public static Bundle<ObjPosition> getDefinition() {
		Bundle<ObjPosition> bundle = new Bundle<>(ObjPosition::new);
		ForeignClass<ObjPosition> def = bundle.definition;

		def.bind("x", getterNum(i -> (double) i.position.x));
		def.bind("y", getterNum(i -> (double) i.position.y));
		def.bind("z", getterNum(i -> (double) i.position.z));

		def.bind("x=(_)", setterNum((i, v) -> i.mut().x = v.floatValue()));
		def.bind("y=(_)", setterNum((i, v) -> i.mut().y = v.floatValue()));
		def.bind("z=(_)", setterNum((i, v) -> i.mut().z = v.floatValue()));

		return bundle;
	}

	private Vector3f mut() {
		if (!mutable) {
			throw new IllegalStateException("Cannot mutate a immutable position!");
		}

		return position;
	}

	@Override
	public String toWrenString() {
		return String.format("vec[%f,%f,%f]",
				position.x,
				position.y,
				position.z
		);
	}
}
