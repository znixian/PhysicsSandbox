/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.physicssandbox.vm;

import io.wren.vm.ForeignClass;
import xyz.znix.physicssandbox.vm.ObjBase.Bundle;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class Pak {

	public final ForeignClass<ObjMicrocontroller> microcontroller;
	public final ForeignClass<ObjCraft> craft;
	public final ForeignClass<ObjPosition> position;
	public final ForeignClass<ObjThruster> thruster;
	public final ForeignClass<ObjPid> pid;
	public final ForeignClass<ObjRotation> rotation;

	public Pak(
			ForeignClass<ObjMicrocontroller> microcontroller,
			ForeignClass<ObjCraft> craft,
			Bundle<ObjPosition> position,
			ForeignClass<ObjThruster> thruster,
			Bundle<ObjPid> pid,
			ForeignClass<ObjRotation> rotation
	) {
		this.microcontroller = microcontroller;
		this.craft = craft;
		this.position = position.bind(this);
		this.thruster = thruster;
		this.pid = pid.bind(this);
		this.rotation = rotation;
	}
}
