/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.physicssandbox.vm;

import com.jme3.math.Quaternion;
import io.wren.value.ForeignMethod.MethodContext;
import io.wren.value.Value;
import io.wren.vm.*;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class ObjRotation extends ObjBase {

	private final ObjPosition forward;
	private final ObjPosition up;

	public ObjRotation(Pak pak, WrenVM vm, Quaternion rotation) {
		super(pak, vm, pak.rotation);
		forward = new ObjPosition(pak, vm, rotation.getRotationColumn(0));
		up = new ObjPosition(pak, vm, rotation.getRotationColumn(1));
	}

	public static ForeignClass<ObjRotation> getDefinition() {
		ForeignClass<ObjRotation> def = new ForeignClass<>();

		def.bind("forward", i -> i::getForward);
		def.bind("up", i -> i::getUp);

		return def;
	}

	private Value getForward(MethodContext ctx, Value[] args) {
		return new Value(forward);
	}

	private Value getUp(MethodContext ctx, Value[] args) {
		return new Value(up);
	}

	@Override
	public String toWrenString() {
		return String.format("rot[%s,%s]",
				forward.toWrenString(),
				up.toWrenString()
		);
	}
}
