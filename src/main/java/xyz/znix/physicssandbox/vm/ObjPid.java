/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.physicssandbox.vm;

import io.wren.value.*;
import io.wren.value.ForeignMethod.MethodContext;
import io.wren.vm.*;
import static xyz.znix.physicssandbox.vm.Utils.*;
import static io.wren.utils.Validate.*;
import xyz.znix.physicssandbox.math.PIDLoop;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class ObjPid extends ObjBase {

	/**
	 * The time this was last updated.
	 */
	private double lastTime;

	/**
	 * The bounds for the output value. Adjusting these scales the output - it
	 * does not change the clipping - adjust the gains for that effect.
	 */
	private double min = -1, max = 1;

	private boolean firstUpdate = true;

	private final PIDLoop pid;

	public ObjPid(Pak pak, WrenVM vm) {
		super(pak, vm, pak.pid);
		pid = new PIDLoop();
	}

	public static Bundle<ObjPid> getDefinition() {
		Bundle<ObjPid> bundle = new Bundle<>(ObjPid::new);
		ForeignClass<ObjPid> def = bundle.definition;

		def.bind("init new()", i -> i::init_0);
		def.bind("init new(_)", i -> i::init_1);

		def.bind("kp", getterNum(i -> i.pid.Kp));
		def.bind("ki", getterNum(i -> i.pid.Ki));
		def.bind("kd", getterNum(i -> i.pid.Kd));
		def.bind("target", getterNum(i -> i.pid.target));

		def.bind("kp=(_)", setterNum((i, v) -> i.pid.Kp = v));
		def.bind("ki=(_)", setterNum((i, v) -> i.pid.Ki = v));
		def.bind("kd=(_)", setterNum((i, v) -> i.pid.Kd = v));
		def.bind("target=(_)", setterNum((i, v) -> i.pid.target = v));

		def.bind("imult", getterNum(i -> i.pid.Imult));
		def.bind("imin", getterNum(i -> i.pid.Imin));
		def.bind("imax", getterNum(i -> i.pid.Imax));

		def.bind("imult=(_)", setterNum((i, v) -> i.pid.Imult = v));
		def.bind("imin=(_)", setterNum((i, v) -> i.pid.Imin = v));
		def.bind("imax=(_)", setterNum((i, v) -> i.pid.Imax = v));

		def.bind("min", getterNum(i -> i.min));
		def.bind("max", getterNum(i -> i.max));

		def.bind("min=(_)", setterNum((i, v) -> i.min = v));
		def.bind("max=(_)", setterNum((i, v) -> i.max = v));

		def.bind("step(_,_)", i -> i::step);
		def.bind("step(_,_,_)", i -> i::step);

		return bundle;
	}

	private Value init_0(MethodContext ctx, Value[] values) {
		return new Value(this);
	}

	private Value init_1(MethodContext ctx, Value[] values) {
		Num(ctx.getCtx(), values[0], "target");
		pid.target = values[0].asDouble();

		return init_0(ctx, values);
	}

	private Value step(MethodContext ctx, Value[] values) {
		// Validate values
		Num(ctx.getCtx(), values[0], "time");
		Num(ctx.getCtx(), values[1], "value");

		// Parse values
		double time = values[0].asDouble();
		double value = values[1].asDouble();

		boolean logging = values.length > 2 && values[2].isTrue();

		// Avoid massive spikes when starting with a high time value
		if (firstUpdate) {
			lastTime = time;
			firstUpdate = false;
		}

		double out = pid.update(time - lastTime, value, logging);
		lastTime = time;

		// Scale output to correct range:
		// Scale to 0-1
		out += 1;
		out /= 2;

		// Scale to desired min/max:
		out *= (max - min);
		out += min;

		return new Value(out);
	}

	@Override
	public String toWrenString() {
		return String.format("pid[TODO toString]");
	}
}
