/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.physicssandbox.vm;

import io.wren.value.ForeignMethod;
import io.wren.value.Value;
import io.wren.vm.ForeignClass;
import io.wren.vm.WrenVM;
import xyz.znix.physicssandbox.craft.blocks.ThrusterBlockBlueprint.BlockThruster;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class ObjThruster extends ObjBase {

	private final BlockThruster thruster;

	public ObjThruster(Pak pak, WrenVM vm, BlockThruster thruster) {
		super(pak, vm, pak.thruster);
		this.thruster = thruster;
	}

	public static ForeignClass<ObjThruster> getDefinition() {
		ForeignClass<ObjThruster> def = new ForeignClass<>();

		def.bind("thrust", i -> i::getThrust);
		def.bind("thrust=(_)", i -> i::setThrust);

		return def;
	}

	@Override
	public String toWrenString() {
		return String.format("thruster[%f]",
				thruster.getThrust()
		);
	}

	private Value getThrust(ForeignMethod.MethodContext mc, Value[] values) {
		return new Value(thruster.getThrust());
	}

	private Value setThrust(ForeignMethod.MethodContext mc, Value[] values) {
		double thrust = values[0].asDouble();
		thrust = Math.min(1, Math.max(0, thrust));
		thruster.setThrust((float) thrust);

		return Value.NULL;
	}
}
