/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.physicssandbox.vm;

import com.jme3.math.*;
import io.wren.value.ForeignMethod.MethodContext;
import io.wren.value.*;
import io.wren.vm.*;
import java.util.*;
import xyz.znix.physicssandbox.craft.SimulatedCraft;
import xyz.znix.physicssandbox.craft.blocks.ThrusterBlockBlueprint.BlockThruster;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class ObjCraft extends ObjBase {

	private final SimulatedCraft craft;
	private final List<Value> thrusters;

	public ObjCraft(Pak pak, WrenVM vm, SimulatedCraft craft) {
		super(pak, vm, pak.craft);
		this.craft = craft;

		thrusters = new ArrayList<>();

		craft.getBlocks().entrySet().stream()
				.map(Map.Entry::getValue)
				.filter(b -> (b instanceof BlockThruster))
				.map(b -> new ObjThruster(pak, vm, (BlockThruster) b))
				.map(Value::new)
				.forEach(thrusters::add);
	}

	public Value position(ForeignMethod.MethodContext ctx, Value[] args) {
		return new Value(new ObjPosition(
				pak, ctx.getVm(),
				craft.getNode().getLocalTranslation().clone()
		));
	}

	public Value velocity(ForeignMethod.MethodContext ctx, Value[] args) {
		return new Value(new ObjPosition(
				pak, ctx.getVm(),
				craft.getPhysics().getLinearVelocity()
		));
	}

	public Value thrusters(ForeignMethod.MethodContext ctx, Value[] args) {
		ObjList list = new ObjList(vm);

		thrusters.forEach(list::add);

		return new Value(list);
	}

	public Value rotation(MethodContext ctx, Value[] args) {
		Quaternion rotation = craft.getPhysics().getPhysicsRotation();
		return new Value(new ObjRotation(pak, vm, rotation));
	}

	public Value angularVelocity(MethodContext ctx, Value[] args) {
		Vector3f vel = craft.getPhysics().getAngularVelocity();
		return new Value(new ObjPosition(pak, vm, vel));
	}

	public static ForeignClass<ObjCraft> getDefinition() {
		ForeignClass<ObjCraft> def = new ForeignClass<>();

		def.bind("position", c -> c::position);
		def.bind("velocity", c -> c::velocity);
		def.bind("rotation", c -> c::rotation);
		def.bind("angularVelocity", c -> c::angularVelocity);
		def.bind("thrusters", c -> c::thrusters);

		return def;
	}

}
