/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.physicssandbox.vm;

import io.wren.value.*;
import io.wren.vm.*;
import java.util.Objects;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public abstract class ObjBase extends Obj {

	protected final Pak pak;

	public ObjBase(Pak pak, WrenVM vm, ForeignClass<? extends ObjBase> def) {
		super(vm);
		this.pak = pak;
		classObj = def.getMapping();
	}

	public String toWrenString() {
		return "[" + getClass().getSimpleName() + " " + Objects.hashCode(this) + "]";
	}

	public static Value toString(ForeignMethod.MethodContext mc, Value[] values) {
		Obj inst = mc.getInstance().asObj();
		if (inst == null) {
			return Value.NULL;
		}

		if (!(inst instanceof ObjBase)) {
			throw new IllegalStateException();
		}

		ObjBase base = (ObjBase) inst;

		return new Value(mc.getVm(), base.toWrenString());
	}

	public static class Bundle<T extends ObjBase> {

		protected final ForeignClass<T> definition;
		protected Pak pak;

		public Bundle(BundledConstructor<T> constructor) {
			this.definition = new ForeignClass<>((vm, oc) -> constructor.construct(pak, vm));
		}

		public ForeignClass<T> bind(Pak pak) {
			Objects.requireNonNull(pak, "Argument 'pak' must not be null!");
			if (this.pak != null) {
				throw new IllegalStateException("pak already set!");
			}
			this.pak = pak;

			return definition;
		}

		public static interface BundledConstructor<T extends ObjBase> {

			T construct(Pak pak, WrenVM vm);
		}
	}

}
