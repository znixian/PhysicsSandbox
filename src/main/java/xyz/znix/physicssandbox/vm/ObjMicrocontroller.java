/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.physicssandbox.vm;

import io.wren.value.ForeignMethod;
import io.wren.value.Value;
import io.wren.vm.ForeignClass;
import io.wren.vm.WrenVM;
import java.util.Objects;
import xyz.znix.physicssandbox.craft.blocks.MicrocontrollerBlockBlueprint.BlockMicrocontroller;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class ObjMicrocontroller extends ObjBase {

	private final BlockMicrocontroller micro;

	public ObjMicrocontroller(Pak pak, WrenVM vm, BlockMicrocontroller micro) {
		super(pak, vm, pak.microcontroller);
		this.micro = micro;
	}

	public static ForeignClass<ObjMicrocontroller> getDefinition() {
		ForeignClass<ObjMicrocontroller> def = new ForeignClass<>();

		def.bind("craft", c -> c::craft);
		def.bind("time", c -> c::time);

		return def;
	}

	public Value craft(ForeignMethod.MethodContext mc, Value[] values) {
		return new Value(new ObjCraft(pak, vm, micro.getCraft()));
	}

	public Value time(ForeignMethod.MethodContext mc, Value[] values) {
		return new Value(micro.getTime());
	}

	public Value update(Value instance) {
		classObj = instance.asClass();
		return new Value(this);
	}

	@Override
	public String toWrenString() {
		return "[microcontroller " + Objects.hashCode(this) + "]";
	}

}
