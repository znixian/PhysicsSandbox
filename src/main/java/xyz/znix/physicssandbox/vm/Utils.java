/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.physicssandbox.vm;

import io.wren.enums.ValueType;
import io.wren.value.*;
import io.wren.vm.ForeignClass.Mapping;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class Utils {

	private Utils() {
	}

	public static <T extends Obj> Mapping<T> getterNum(Function<T, Double> o) {
		return (instance, ctx, args) -> {
			return new Value(o.apply(instance));
		};
	}

	public static <T extends Obj> Mapping<T> getterObj(Function<T, Obj> o) {
		return (instance, ctx, args) -> {
			Obj val = o.apply(instance);

			return val == null ? Value.NULL : new Value(val);
		};
	}

	public static <T extends Obj> Mapping<T> setterNum(BiConsumer<T, Double> o) {
		return (instance, ctx, args) -> {
			Value val = args[0];

			if (!val.isNum()) {
				throw new IllegalArgumentException("Value '" + val + "' must be a number!");
			}

			o.accept(instance, val.asDouble());

			return val;
		};
	}

	public static <T extends Obj> Mapping<T> setterObj(BiConsumer<T, Obj> o) {
		return (instance, ctx, args) -> {
			Value val = args[0];

			if (val.isNull()) {
				o.accept(instance, null);
				return val;
			}

			if (val.getType() != ValueType.OBJ) {
				throw new IllegalArgumentException("Value '" + val + "' must be a object or null!");
			}

			o.accept(instance, val.asObj());

			return val;
		};
	}
}
