package xyz.znix.physicssandbox;

/**
 * Created by znix on 7/1/17.
 */
public class Constants {
	public static final float BASE_MOVE_SPEED = 2;
	public static final float EXP_MOVE_SPEED_MULT = 0.5f;

	private Constants() {
	}
}
