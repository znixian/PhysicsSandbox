package xyz.znix.physicssandbox;

import xyz.znix.physicssandbox.craft.CraftDescriptor;

import java.io.*;

/**
 * Created by znix on 29/06/17.
 */
public class SaveSystem {
	private static final File CRAFT_FILE = new File("craft.save");

	public void save(CraftDescriptor craft) {
		try (ObjectOutputStream out = new ObjectOutputStream(
				new BufferedOutputStream(new FileOutputStream(CRAFT_FILE)))) {
			out.writeObject(craft);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public CraftDescriptor load() {
		try (ObjectInputStream out = new ObjectInputStream(
				new BufferedInputStream(new FileInputStream(CRAFT_FILE)))) {
			return (CraftDescriptor) out.readObject();
		} catch (FileNotFoundException ex) {
			System.out.println("No craft found!");
			return null;
		} catch (IOException | ClassNotFoundException e) {
			throw new RuntimeException("Cannot load craft!", e);
		}
	}
}
