package xyz.znix.physicssandbox.craft;

import com.jme3.scene.Spatial;

/**
 * Created by znix on 7/1/17.
 */
public interface BlockInformationProvider {
	float BLOCK_SIZE = 2;

	float getMass();

	Spatial createNode(Vector3i pos, NodeBasedCraft.AssetResolver assets);
}
