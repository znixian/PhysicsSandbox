package xyz.znix.physicssandbox.craft;

import xyz.znix.physicssandbox.craft.blocks.MicrocontrollerBlockBlueprint;
import xyz.znix.physicssandbox.craft.blocks.StructuralBlockBlueprint;
import xyz.znix.physicssandbox.craft.blocks.ThrusterBlockBlueprint;

import java.util.function.Supplier;

/**
 * Represents a type of block. There is a 1:1 relationship with {@link BlockBlueprint}.
 * <p>
 * Created by znix on 27/06/17.
 */
public enum BlockType {
	STRUCTURAL(StructuralBlockBlueprint::new, "structural.png"),
	THRUSTER(ThrusterBlockBlueprint::new, "thruster.png"),
	MICROCONTROLLER(MicrocontrollerBlockBlueprint::new, "microcontroller.png");

	private final Supplier<BlockBlueprint> producer;
	private final String texture;

	BlockType(Supplier<BlockBlueprint> producer, String texture) {
		this.producer = producer;
		this.texture = texture;
	}

	/**
	 * Creates a new block.
	 *
	 * @return The new block of this type.
	 */
	public BlockBlueprint create() {
		return producer.get();
	}

	public String getTexture() {
		return texture;
	}
}
