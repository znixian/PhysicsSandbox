package xyz.znix.physicssandbox.craft;

import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.texture.Texture;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * Created by znix on 27/06/17.
 */
public abstract class BlockBlueprint implements Externalizable, BlockInformationProvider {
	public static final int serialVersionUID = 1707011332; // 2015 07 01 13:32

	protected transient final BlockType type;

	public BlockBlueprint(BlockType type) {
		this.type = type;
	}

	public final BlockType getType() {
		return type;
	}

	public boolean isTickable() {
		return false;
	}

	public abstract Block construct();

	public float getMass() {
		return 1;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
	}

	public Spatial createNode(Vector3i pos, NodeBasedCraft.AssetResolver assets) {
		// create cube shape
		float size = BLOCK_SIZE / 2;
		Box b = new Box(size, size, size);

		// create cube geometry from the shape
		Geometry geom = new Geometry("Box", b);

		// Set the position
		Vector3f objPos = geom.getLocalTranslation();
		pos.toFloat(objPos).multLocal(BLOCK_SIZE);

		// Build the material
		Material mat = assets.getMaterial(null).clone();

		// Get or load the texture
		Texture tex = assets.getTexture("Textures/blocks/" + getType().getTexture());
		mat.setTexture("ColorMap", tex);

		// Set the material
		geom.setMaterial(mat);

		// Hand out the geometry
		return geom;
	}
}
