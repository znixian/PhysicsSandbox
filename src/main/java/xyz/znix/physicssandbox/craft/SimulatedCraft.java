package xyz.znix.physicssandbox.craft;

import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Represents a craft while it is being simulated.
 * <p>
 * Created by znix on 27/06/17.
 */
public class SimulatedCraft implements NodeBasedCraft {
	private final RigidBodyControl physics;
	private final Node node;
	private final Map<Vector3i, Block> blocks;
	private final Vector3f centerOfMass;

	public SimulatedCraft(CraftDescriptor craft, RigidBodyControl physics,
						  Node node, Vector3f centerOfMass) {
		this.physics = physics;
		this.node = node;
		this.centerOfMass = centerOfMass;

		Map<Vector3i, Block> blocks = new HashMap<>();

		for (Map.Entry<Vector3i, BlockBlueprint> entry : craft.getBlocks().entrySet()) {
			Block b = entry.getValue().construct();
			blocks.put(entry.getKey(), b);
		}

		this.blocks = Collections.unmodifiableMap(blocks);
	}

	public SimulatedCraft(RigidBodyControl physics, Node node,
						  Map<Vector3i, Block> blocks, Vector3f centerOfMass) {
		this.physics = physics;
		this.node = node;
		this.blocks = Collections.unmodifiableMap(blocks);
		this.centerOfMass = centerOfMass;
	}

	public RigidBodyControl getPhysics() {
		return physics;
	}

	public Node getNode() {
		return node;
	}

	public Map<Vector3i, Block> getBlocks() {
		return blocks;
	}

	public Vector3f getCenterOfMass() {
		return centerOfMass;
	}

	/**
	 * Find any seperate groups of parts inside this craft. If this
	 * craft is a single part, {@code null} is returned. Otherwise,
	 * the list of parts is returned.
	 */
	public List<? extends Collection<Vector3i>> findAllCrafts() {
		if (blocks.isEmpty()) {
			throw new IllegalStateException("Cannot be an empty craft!");
		}

		List<Set<Vector3i>> partGroups = new ArrayList<>();
		Set<Vector3i> mainParts = new HashSet<>(blocks.keySet());

		while (!mainParts.isEmpty()) {
			Set<Vector3i> partList = new HashSet<>();
			Set<Vector3i> nextTodo = new HashSet<>();
			Set<Vector3i> todo = new HashSet<>();

			// Get a part to start us off
			{
				Vector3i firstPart = mainParts.iterator().next();
				mainParts.remove(firstPart);
				todo.add(firstPart);
			}

			while (!todo.isEmpty()) {
				// Empty out the future list
				nextTodo.clear();

				// Add these parts
				partList.addAll(todo);

				// Find their neighbours
				for (Vector3i pos : todo) {
					for (Vector3i around : pos.adjacent()) {
						if (mainParts.contains(around)) {
							mainParts.remove(around);
							nextTodo.add(around);
						}
					}
				}

				// Shift across
				{
					Set<Vector3i> tmp = nextTodo;
					nextTodo = todo;
					todo = tmp;
				}
			}

			partGroups.add(partList);
		}

		if (partGroups.size() == 1) {
			return null;
		}

		return partGroups;
	}

	@Override
	public void createNodes(AssetResolver ar, Consumer<Spatial> consumer) {
		NodeBasedCraft.createNodes(ar, blocks, consumer);
	}
}
