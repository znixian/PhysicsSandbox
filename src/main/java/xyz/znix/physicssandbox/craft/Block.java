package xyz.znix.physicssandbox.craft;

import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

/**
 * Created by znix on 27/06/17.
 */
public abstract class Block implements BlockInformationProvider {
	private final BlockBlueprint blueprint;

	public Block(BlockBlueprint blueprint) {
		this.blueprint = blueprint;
	}

	public void update(SimulatedCraft craft, Vector3i position, Vector3f physicalPos, float tpf) {
		throw new UnsupportedOperationException("isTickable() should return false!");
	}

	public BlockBlueprint getBlueprint() {
		return blueprint;
	}

	@Override
	public float getMass() {
		return blueprint.getMass();
	}

	@Override
	public Spatial createNode(Vector3i pos, NodeBasedCraft.AssetResolver assets) {
		return blueprint.createNode(pos, assets);
	}
}
