package xyz.znix.physicssandbox.craft.blocks;

import com.jme3.math.Vector3f;
import xyz.znix.physicssandbox.craft.*;
import xyz.znix.physicssandbox.vm.*;
import io.wren.enums.WrenInterpretResult;
import io.wren.value.*;
import io.wren.vm.*;

import java.io.*;
import java.util.logging.Logger;

public class MicrocontrollerBlockBlueprint extends BlockBlueprint {

	private static final Logger LOGGER = Logger.getLogger(MicrocontrollerBlockBlueprint.class.getName());
	private static final String API_SCRIPT;
	private static final int TICKS_PER_SECOND = 1000;
	private static final float TICK_TIME = 1f / TICKS_PER_SECOND;

	static {
		API_SCRIPT = readResource("microcontroller");
	}

	private static String readResource(String name) {
		InputStream in = MicrocontrollerBlockBlueprint.class.getResourceAsStream("/ScriptApi/" + name + ".w");
		StringBuilder str = new StringBuilder();
		try (Reader r = new BufferedReader(new InputStreamReader(in))) {

			char chars[] = new char[1024];
			while (true) {
				int read = r.read(chars, 0, chars.length);
				if (read == -1) {
					break;
				}
				str.append(chars, 0, read);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return str.toString();
	}

	/**
	 * The user-editable representation of this program.
	 */
	private final StringBuffer program;

	public MicrocontrollerBlockBlueprint() {
		super(BlockType.MICROCONTROLLER);

		program = new StringBuffer(
				API_SCRIPT + "\n" + readResource("program")
		);
	}

	@Override
	public Block construct() {
		return new BlockMicrocontroller();
	}

	@Override
	public boolean isTickable() {
		return true;
	}

	public class BlockMicrocontroller extends Block {

		private final WrenVM vm;
		private final ObjFiber mainThread;
		private WrenInterpretResult state;
		private SimulatedCraft craft;
		private float time;
		private int ticksRun;

		// Wren representations of stuff
		private ObjMicrocontroller microcontroller;

		private BlockMicrocontroller() {
			super(MicrocontrollerBlockBlueprint.this);
			vm = new WrenVM();

			loadApi(vm.getFFI());
			// TODO implement import
//			vm.interpret("api", API_SCRIPT);

			mainThread = vm.loadModule("userscript", program.toString());
			if (mainThread == null) {
				throw new IllegalStateException("Program did not compile!");
			}
			vm.setupInterpreter(mainThread);
		}

		private void loadApi(FFI ffi) {
			final String MOD = "userscript"; //"api";

			// TODO should we use a single instance?
			ForeignClass<ObjMicrocontroller> def = ObjMicrocontroller.getDefinition();

			Pak pak = new Pak(
					def,
					ObjCraft.getDefinition(),
					ObjPosition.getDefinition(),
					ObjThruster.getDefinition(),
					ObjPid.getDefinition(),
					ObjRotation.getDefinition()
			);

			microcontroller = new ObjMicrocontroller(pak, vm, this);
			def.addMethod("me", (ctx, args) -> microcontroller.update(ctx.getInstance()));

			ffi.bind(MOD, "Base", "toString", ObjBase::toString);

			ffi.bind(MOD, "Microcontroller", pak.microcontroller);
			ffi.bind(MOD, "Craft", pak.craft);
			ffi.bind(MOD, "Pos", pak.position);
			ffi.bind(MOD, "Thruster", pak.thruster);
			ffi.bind(MOD, "PID", pak.pid);
			ffi.bind(MOD, "Rotation", pak.rotation);
		}

		@Override
		public void update(SimulatedCraft craft, Vector3i position, Vector3f physicalPos, float tpf) {
			if (state != null) {
				return;
			}

			time += tpf;

			this.craft = craft;

			while (time > ticksRun * TICK_TIME) {
				// TODO should we just catch RuntimeExceptions, as that's what this throws?
				state = vm.runStep();

				if (state != null) {
					System.out.println("Finished with " + state);
					break;
				}

				ticksRun++;
			}
		}

		public SimulatedCraft getCraft() {
			return craft;
		}

		public float getTime() {
			return time;
		}
	}
}
