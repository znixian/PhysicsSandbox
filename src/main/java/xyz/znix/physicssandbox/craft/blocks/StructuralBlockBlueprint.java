package xyz.znix.physicssandbox.craft.blocks;

import xyz.znix.physicssandbox.craft.Block;
import xyz.znix.physicssandbox.craft.BlockBlueprint;
import xyz.znix.physicssandbox.craft.BlockType;

/**
 * Created by znix on 27/06/17.
 */
public class StructuralBlockBlueprint extends BlockBlueprint {
	public StructuralBlockBlueprint() {
		super(BlockType.STRUCTURAL);
	}

	@Override
	public Block construct() {
		return new Impl();
	}

	private class Impl extends Block {
		private Impl() {
			super(StructuralBlockBlueprint.this);
		}
	}
}
