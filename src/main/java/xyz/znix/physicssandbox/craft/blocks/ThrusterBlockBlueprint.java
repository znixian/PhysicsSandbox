package xyz.znix.physicssandbox.craft.blocks;

import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import xyz.znix.physicssandbox.craft.*;

/**
 * Created by znix on 27/06/17.
 */
public class ThrusterBlockBlueprint extends BlockBlueprint {
	public ThrusterBlockBlueprint() {
		super(BlockType.THRUSTER);
	}

	@Override
	public boolean isTickable() {
		return true;
	}

	@Override
	public Block construct() {
		return new BlockThruster();
	}

	public class BlockThruster extends Block {

		private float thrust;

		private BlockThruster() {
			super(ThrusterBlockBlueprint.this);
			thrust = 1;
		}

		@Override
		public void update(SimulatedCraft craft, Vector3i position, Vector3f physicalPos, float tpf) {
			RigidBodyControl physics = craft.getPhysics();

			Quaternion rotation = physics.getPhysicsRotation();
			Vector3f thrustDir = new Vector3f(0, 50, 0).multLocal(this.thrust);
			rotation.multLocal(thrustDir);
			rotation.multLocal(physicalPos);

			physics.applyForce(thrustDir, physicalPos);
		}

		public float getThrust() {
			return thrust;
		}

		public void setThrust(float thrust) {
			this.thrust = thrust;
		}
	}

}
