package xyz.znix.physicssandbox.craft;

import com.jme3.math.Vector3f;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * A integer-based 3D Vector.
 * <p>
 * Created by znix on 27/06/17.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class Vector3i implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final Vector3i ZERO = new Vector3i(0, 0, 0);

	private final int x, y, z;

	public Vector3i(Vector3f other) {
		this((int) other.x, (int) other.y, (int) other.z);
	}

	public Vector3i(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	// Add
	public Vector3i add(int x, int y, int z) {
		return new Vector3i(this.x + x, this.y + y, this.z + z);
	}

	public Vector3i add(Vector3i other) {
		return add(other.x, other.y, other.z);
	}

	// Subtract
	public Vector3i subtract(int x, int y, int z) {
		return add(-x, -y, -z);
	}

	public Vector3i subtract(Vector3i other) {
		return subtract(other.x, other.y, other.z);
	}

	// XYZ Getters
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getZ() {
		return z;
	}

	// toString, equals and hashCode
	@Override
	public String toString() {
		return "Vector3i{" +
				"x=" + x +
				", y=" + y +
				", z=" + z +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Vector3i vector3i = (Vector3i) o;

		if (x != vector3i.x) return false;
		if (y != vector3i.y) return false;
		return z == vector3i.z;
	}

	@Override
	public int hashCode() {
		int result = x;
		result = 31 * result + y;
		result = 31 * result + z;
		return result;
	}

	// To JME Vector3f
	public Vector3f toFloat() {
		return toFloat(new Vector3f());
	}

	public Vector3f toFloat(Vector3f tmp) {
		return tmp.set(x, y, z);
	}

	// Multiplication
	public Vector3i mult(int x, int y, int z) {
		return new Vector3i(this.x * x, this.y * y, this.z * z);
	}

	public Vector3i mult(Vector3i other) {
		return mult(other.x, other.y, other.z);
	}

	public Vector3i mult(int scalar) {
		return mult(scalar, scalar, scalar);
	}

	public List<Vector3i> adjacent() {
		return Arrays.asList(
				add(+1, 0, 0),
				add(-1, 0, 0),
				add(0, +1, 0),
				add(0, -1, 0),
				add(0, 0, +1),
				add(0, 0, -1)
		);
	}
}
