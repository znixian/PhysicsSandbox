package xyz.znix.physicssandbox.craft;

import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import com.jme3.texture.Texture;

import java.util.Map;
import java.util.function.Consumer;

/**
 * Created by znix on 7/1/17.
 */
public interface NodeBasedCraft {
	Map<Vector3i, ? extends BlockInformationProvider> getBlocks();

	default float getMass() {
		return NodeBasedCraft.getMass(getBlocks());
	}

	static float getMass(Map<Vector3i, ? extends BlockInformationProvider> nodes) {
		return nodes.values().stream().map(BlockInformationProvider::getMass).reduce((a, b) -> a + b).orElse(0f);
	}

	default Vector3f getCenterOfMass() {
		return NodeBasedCraft.getCenterOfMass(getBlocks());
	}

	static Vector3f getCenterOfMass(Map<Vector3i, ? extends BlockInformationProvider> nodes) {
		Vector3f com = Vector3f.ZERO.clone();
		Vector3f tmp = new Vector3f();
		float totalMass = 0;

		for (Map.Entry<Vector3i, ? extends BlockInformationProvider> entry : nodes.entrySet()) {
			Vector3i pos = entry.getKey();
			float localMass = entry.getValue().getMass(); // All nodes are the same mass

			pos.toFloat(tmp);
			tmp.multLocal(localMass);
			com.addLocal(tmp);

			totalMass += localMass;
		}

		com.divideLocal(totalMass);

		return com;
	}


	void createNodes(AssetResolver ar, Consumer<Spatial> consumer);

	static void createNodes(AssetResolver ar, Map<Vector3i, ? extends BlockInformationProvider> blocks,
							Consumer<Spatial> consumer) {

		for (Vector3i pos : blocks.keySet()) {
			Spatial node = blocks.get(pos).createNode(pos, ar);
			consumer.accept(node);
		}
	}

	interface AssetResolver {
		Texture getTexture(String name);

		Material getMaterial(String name);
	}
}
