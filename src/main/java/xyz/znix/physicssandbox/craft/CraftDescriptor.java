package xyz.znix.physicssandbox.craft;

import com.jme3.scene.Spatial;
import xyz.znix.physicssandbox.craft.blocks.StructuralBlockBlueprint;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Created by znix on 27/06/17.
 */
public class CraftDescriptor implements Serializable, NodeBasedCraft {
	private Map<Vector3i, BlockBlueprint> nodes;

	public CraftDescriptor() {
		nodes = new HashMap<>();
		nodes.put(Vector3i.ZERO, new StructuralBlockBlueprint());
	}

	public Map<Vector3i, BlockBlueprint> getBlocks() {
		return nodes;
	}

	@Override
	public void createNodes(AssetResolver ar, Consumer<Spatial> consumer) {
		NodeBasedCraft.createNodes(ar, nodes, consumer);
	}

}
