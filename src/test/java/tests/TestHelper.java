package tests;

import static org.junit.Assert.*;

/**
 * Created by znix on 27/06/17.
 */
public class TestHelper {
	private TestHelper() {
	}

	public static ThrowableAssertion assertThrown(ExceptionThrower exceptionThrower) {
		try {
			exceptionThrower.throwException();
		} catch (Throwable caught) {
			return new ThrowableAssertion(caught);
		}
		fail("Exception not thrown!");
		throw new RuntimeException(); // Should never get here
	}

	public static class ThrowableAssertion {

		private final Throwable caught;

		public ThrowableAssertion(Throwable caught) {
			this.caught = caught;
		}

		public ThrowableAssertion isInstanceOf(Class<? extends Throwable> exceptionClass) {
			assertTrue(
					"Exception must be instance of " + exceptionClass,
					exceptionClass.isInstance(caught)
			);
			return this;
		}

		public ThrowableAssertion hasMessage(String expectedMessage) {
			assertEquals(caught.getMessage(), expectedMessage);
			return this;
		}

		public ThrowableAssertion hasNoCause() {
			assertNull(caught.getCause());
			return this;
		}

		public ThrowableAssertion hasCauseInstanceOf(Class<? extends Throwable> exceptionClass) {
			assertNotNull(caught.getCause());
			assertTrue(
					"Exception must be instance of " + exceptionClass,
					exceptionClass.isInstance(caught.getCause())
			);
			return this;
		}
	}

	public interface ExceptionThrower {
		void throwException() throws Throwable;
	}
}
