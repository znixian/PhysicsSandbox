package tests;

import org.junit.Test;
import xyz.znix.physicssandbox.craft.Block;
import xyz.znix.physicssandbox.craft.BlockBlueprint;
import xyz.znix.physicssandbox.craft.BlockType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static tests.TestHelper.assertThrown;

/**
 * Created by znix on 27/06/17.
 */
public class BlockTest {
	@Test
	public void testBlockTypeMappings() {
		for (BlockType type : BlockType.values()) {
			BlockBlueprint blueprint = type.create();
			assertEquals("BlockType and BlockBlueprint should have a 1:1 mapping",
					type, blueprint.getType());

			Block block = blueprint.construct();
			assertNotNull("Block " + type + " cannot construct to NULL", block);
			assertEquals(block.getBlueprint(), blueprint);
		}
	}

	@Test
	public void testNoOverriddenUpdates() {
		for (BlockType type : BlockType.values()) {
			BlockBlueprint blueprint = type.create();
			if (!blueprint.isTickable()) {
				assertThrown(() -> blueprint.construct().update(
						null, null, null, 1f / 60f
				))
						.hasNoCause()
						.isInstanceOf(UnsupportedOperationException.class)
						.hasMessage("isTickable() should return false!");
			}
		}
	}
}
